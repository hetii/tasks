#!/usr/bin/env python
from autobahn.twisted.wamp import ApplicationSession
from autobahn.twisted.util import sleep
from twisted.internet.defer import inlineCallbacks
import pyinotify, subprocess, glob, datetime
from twisted.internet import inotify
from flask_version import get_schedulers, set_schedulers
from twisted.python import filepath

class MyComponent(ApplicationSession):
    fqdn = "example.com"

    #Remote Procedure Calls
    def rpc_get_schedulers(self):
        return get_schedulers(False)

    def rpc_set_schedulers(self, drive_name, schedname):
        print drive_name, schedname
        return set_schedulers({drive_name:schedname})

    #Publish & Subscribe part
    def pubsrv_scheduler_change(self, *args, **kwargs):
        self.publish(u'com.example.scheduler_change', self.rpc_get_schedulers())

    @inlineCallbacks
    def onJoin(self, details):
        print("Session ready...")
        #helper to register all our RPC methods.
        try:
            for name in dir(self):
                if name.startswith('rpc_'):
                    hn = '.'.join(self.fqdn.split('.')[::-1]) #host name
                    fn = name.partition('_')[2]               #procedure name
                    print("Register new RPC method: %s.%s" % (hn, fn))
                    yield self.register(getattr(self, name), '%s.%s' % (hn, fn))
        except Exception as e:
            print("failed to register procedure: {}".format(e))
        else:
            print("procedure registered")

        notifier = inotify.INotify()
        notifier.startReading()
        for dev in glob.glob('/sys/block/[csh]*/queue/scheduler'):
            notifier.watch(filepath.FilePath(dev), mask=pyinotify.IN_CLOSE_WRITE, callbacks=[self.pubsrv_scheduler_change])

if __name__ == '__main__':
    from autobahn.twisted.wamp import ApplicationRunner
    runner = ApplicationRunner("ws://127.0.0.1:9000/ws", "realm1", debug=True)
    runner.run(MyComponent)

