# TIETO task #

Hi, general I wrote two version of this task.
First handle scheduler by simple RESTful client written in Flask web framework.
RESTful is nice, well documented way to handle data flow but this days where we wave native web-socket its a bit outdated.

That`s why I also write a second example based on web-socket and autobahn project.
In second scenario we handle scheduler in asynchronous way and get very fast and efficient way our changes in website.

In this version I also use inotify event system to watch if other external vendor change schedulers.
If such things happens you will see this changes in web browser.

Below short instruction how to handle it:

---
	Pre-installation:
	To compile properly crossbar system please install libffi-dev packages: 
	for debian like system: apt-get install libffi-dev

---
    Installation:
    1. virtualenv -p /usr/bin/python2.7 --no-site-packages venv
    2. cd venv && source bin/activate
    3. git clone https://bitbucket.org/hetii/tasks.git
    4. cd tasks && pip install -r requirements.txt

---
	Usage:
	1. For flask version.
		a) run in terminal 1: sudo ./flask_version.py
		b) run in terminal 2: watch -x cat /sys/block/[csh]*/queue/scheduler
		c) calls: curl http://localhost/scheduler/sda -X GET -v
		d) calls: curl http://localhost/scheduler/sda -d "sched_name=noop" -X PUT -v
		e) calls: curl http://localhost/scheduler -d "sda=cfq" -d "sdc=noop" -X POST -v

	2. For autobahn version.
		a) run in terminal 1: crossbar start
		b) run in termianl 2: sudo ./autobahn_version.py
		c) open in browser  : autobahn_index.html
		d) change states via browser or via terminal by flask version or by shell.