
var Table = Reactable.Table;
var View = React.createClass({displayName: "View",
    //Handler used to execute RPC call in remote side.
    //Main purpose is to update scheduler.
    changeHandler:function (e){
        this.props.autobahn.call('com.example.set_schedulers', [e.target.id, e.target.value] ).then(
            null,
            function (error) {
                alert("Error from server:" + error.args[1]);
                console.log("Call failed:", error);
            }
        );
    },
    //Local helper that provide data for our table with current states.
    updateTable: function (data){
        if (data[0] != undefined){
            data = data[0]
        }
        var d = [];
        for (var key in data) {
          if (data.hasOwnProperty(key)) {
            console.log(key + " -> " + data[key]); 
            var a =(
            <select onChange={this.changeHandler} id={key}>
              <option value="noop">Noop</option>
              <option value="deadline">Deadline</option>
              <option value="cfq">Cfq</option>
            </select>);
            d.push({'Disk':key, 'State':data[key], 'Scheduler':a});
          }
        }
        this.setState({table_data:d});
    },
    //This is our init section where we made our first rpc call 
    //and we also subscribe to pub&service to get events 
    //if some drive scheduler are updated by external source. 
    componentDidMount: function() {
        this.props.autobahn.subscribe('com.example.scheduler_change', this.updateTable);
        this.props.autobahn.call('com.example.get_schedulers').then(
            this.updateTable,
            function (error) {
             console.log("Call failed:", error);
            }
        );
    },
    //Get initial state used in project.
    getInitialState: function() {
        return {
            table_data:[],
        };
    },
    //Rerender our table and populate with data based on current state.
    render: function() {
        return (
            <div>
                <Table className="table" data={this.state.table_data} />
            </div>
        );
  }
});

try {
   var autobahn = require('autobahn');
} catch (e) {
   // when running in browser, AutobahnJS will
   // be included without a module system
}
 
var connection = new autobahn.Connection({
   url: 'ws://127.0.0.1:9000/ws',
   realm: 'realm1'}
);
 
connection.onopen = function (session) {
    //After connecting we bind our React component to body element.
    React.render(<View autobahn={session}/>, document.getElementById('body_wrapper'));
};
 
connection.open();