#!/usr/bin/env python

import glob, re, json
from flask import Flask
from flask.ext.restful import reqparse, abort, Api, Resource

def get_schedulers(to_json=True):
    """get_schedulers() -> All disk name with a current scheduler in json format.
       get_schedulers(False) -> All disk name with a current scheduler as dict object.
    """
    res = {}
    #Get all hard disk names in system. I just focus here on devices like sda/hda/scsi.
    for dev in glob.glob('/sys/block/[csh]*/queue/scheduler'):
        #Populate our dictonary with a current value of disk scheduler.
        #Keep it simple without try/cath on failure.
        res[dev.split('/')[3]] = re.search('\[(.*)\]', file(dev, 'r').read()).group(1)
    #Convert out dictonary to json format.
    return json.dumps(res) if to_json else res

def set_schedulers(sched_data):
    """set_schedulers({'sdc':'deadline'}) -> Set scheduler to 'deadline' for sdc.
       Accept as input dictonary type or his json representation string.
       Raise ValueError when wrong schedule name is given.
    """
    schednames = ['noop', 'deadline', 'cfq']
    #Convert json data to dictonary if needed.
    if isinstance(sched_data, str):
        sched_data = json.loads(sched_data)
    #Set schedule for each disk and raise ValueError when wrong schedule name is given.
    #To keep it simple I don`t check here if drive path is valid and accessible. Assume to yes...
    for k, v in sched_data.items():
        if not v in schednames:
            raise ValueError("Wrong schedule name: %s, valid are: %s" % (v,', '.join(schednames)))
        file('/sys/block/%s/queue/scheduler' % k, 'w').write(v)

#I rid from this class get_schedulers/set_schedulers to be able use them in other code.
#I could put them here also and mark them as a static method by @staticmethod decorator, 
#but finaly decide to have them out.
class SchedulerManager(Resource):
    """
        This is a basic resource to handle disk scheduler in linux sysfile.
    """

    def __init__(self, *args, **kwargs):
        #Prepare parser with all possible arguments.
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('sched_name', type=str)

        for k in get_schedulers(False):
            self.parser.add_argument(k, type=str)
        
        return super(SchedulerManager, self).__init__(*args, **kwargs)

class Scheduler(SchedulerManager):
    """
        This is a subclass resource to handle disk scheduler for one drive.
    """

    def get(self, dev_name):
        """Return scheduler schema about specyfic drive.
           eg call: curl http://localhost/scheduler/sda -X GET -v
        """
        scheds = get_schedulers(False)
        if dev_name not in scheds:
            abort(404, message="Device {} doesn't exist".format(dev_name))
        return scheds[dev_name]

    def put(self, dev_name):
        """Update scheduler schema for single drive.
            eg call: curl http://localhost/scheduler/sda -d "sched_name=noop" -X PUT -v
        """
        args = self.parser.parse_args()
        try:
            #Get single drive name and update his scheduler,
            #for any fails return 500 http response with proper information.
            sched_data = {dev_name:args.get('sched_name')}
            set_schedulers(sched_data)
        except Exception, e:
            abort(500, message=e.message or e.strerror)
        return sched_data, 201


class SchedulerList(SchedulerManager):
    """
        This is a basic resource to handle multiple schedule setting for all drives.
    """

    def get(self):
        """Return current schedule settings for all drives.
           eg call: curl http://localhost/scheduler -X GET -v
        """
        return get_schedulers(False)
    #This http method usualy is used to CREATE objects in REST world  
    #but here I decide to use it just to handle multiple arguments.
    def post(self):
        """Update scheduler schema for multiple drive.
           eg call: curl http://localhost/scheduler -d "sda=cfq" -d "sdc=noop" -X POST -v
        """
        args = self.parser.parse_args()
        try:
            #Ok, here a bit lazy way to filter out all not needed values from args,
            #and just keep real valid keywors value as a drive_name:schedule_name.
            sched_data = dict([(k,v) for k,v in args.items() if v and k != 'sched_name'])
            set_schedulers(sched_data)
        except Exception, e:
            abort(500, message=e.message or e.strerror)
        return sched_data, 201

if __name__ == '__main__':
    app = Flask(__name__)
    api = Api(app)

    #Setup the api resource routing 
    api.add_resource(SchedulerList, '/scheduler')
    api.add_resource(Scheduler, '/scheduler/<dev_name>')
    #Start listen on localhost interfave and port 80.
    #For most linux system port 80 will required root permissions.
    #BTW playing with sysfs also ... 
    app.run(debug=True, host='0.0.0.0', port=80)